﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AKQATest.Service;

namespace UnitTestProject
{
    [TestClass]
    public class CurrencyToWordsUnitTest
    {
        [TestMethod]
        public void ConvertToWords()
        {
            var cs = new CurrencyToWordsService();

            var result = cs.NumberToWords(123.45);

            Assert.AreEqual("one hundred twenty-three dollars and forty-five cents", result, true);
        }
    }
}
