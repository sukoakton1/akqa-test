﻿using AKQATest.Models;
using AKQATest.Service;
using System.Web.Mvc;

namespace AKQATest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(PriceWordsModel currency)
        {
            var service = new CurrencyToWordsService(); //TODO: should have used dependency injector to get object
            var words = service.NumberToWords(currency.Price);

            var result = new AjaxResponse();

            if (string.IsNullOrEmpty(words))
            {
                result.IsSuccess = false;
                result.Message = "Error";
                result.Response = words;
                return Json(result);
            }
            else
            {
                result.IsSuccess = true;
                result.Message = "Price to words successfull";
                result.Response = words;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}