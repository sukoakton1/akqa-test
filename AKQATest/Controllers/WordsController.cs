﻿using AKQATest.Models;
using AKQATest.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AKQATest.Controllers
{
    public class NumberToWordsController : ApiController
    {
        [HttpPost]
        [Route("akqa/currencytowords")]
        public IHttpActionResult ToWords(PriceWordsModel currency)
        {
            var service = new CurrencyToWordsService();
            var words = service.NumberToWords(currency.Price);

            var result = new AjaxResponse { IsSuccess = true, Message = "Price to words successfull", Response = words };

            return Ok(result);
        }
    }
}