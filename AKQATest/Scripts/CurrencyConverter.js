﻿$(document).ready(function () {
    console.log("ready!");

    $('#btnSubmit').click(function (event) {
        event.preventDefault();

        console.log("button clicked");

        var name = $('#Name').val();

        $.ajax({
            type: "POST",
            url: 'akqa/currencytowords',
            data: $('#frmCurrency').serialize(),
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("success");
                console.log(data.Response);
                $('#lblResult').text(name + ', ' + data.Response);
            },
            error: function (data, textStatus, error) {
                $('#lblResult').text('Error occured, please try later !');
            }            
        });

    });

    $("#Price").keydown(function (e) {
        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||        
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||        
            (e.keyCode >= 35 && e.keyCode <= 40)) {

            return;
        }
        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});