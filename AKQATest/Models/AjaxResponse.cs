﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AKQATest.Models
{
    public class AjaxResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public object Response { get; set; }
    }
}