﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AKQATest.Models
{
    public class PriceWordsModel
    {
        [Required(ErrorMessage = "First Name Required")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Price Required")]
        public double Price { get; set; }
    }
}